# jupyter-nerfstudio-proxy
Integrate Nerfstudio viewer in the Jupyter environment.

## Requirements
- Python 3.7+
- Jupyter Notebook 6.0+
- JupyterLab 3.0+

This package spins up a web server that is serving nerfstudio viewer static content.

## Jupyter-Server-Proxy
[Jupyter-Server-Proxy](https://jupyter-server-proxy.readthedocs.io) lets you run arbitrary external processes (such as MLflow) alongside your notebook, and provide authenticated web access to them.

## Install

#### Create and Activate Environment
```
virtualenv -p python3 venv
source venv/bin/activate
```

#### Install jupyter-nerfstudio-proxy
```
pip install git+https://gitlab.com/idris-cnrs/jupyter/jupyter-proxy-apps/jupyter-nerfstudio-proxy.git
```
