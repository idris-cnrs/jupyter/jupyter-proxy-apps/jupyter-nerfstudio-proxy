"""Simple import and function call test"""


def test_setupcall():
    """
    Test the call of the setup function
    """
    import jupyter_nerfstudio_proxy as jn

    print("Running test_setupcall...")
    print(jn.setup_nerfstudio_viewer())
