# Copyright 2023 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
import logging
import shutil

from typing import Any
from typing import Dict

from ._version import __version__  # noqa


def get_logger(name):
    """Configure logging"""
    logger = logging.getLogger(name)
    if not logger.handlers:
        # Prevent logging from propagating to the root logger
        logger.propagate = 0
        console = logging.StreamHandler()
        logger.addHandler(console)
        formatter = logging.Formatter(
            '[%(levelname).1s %(asctime)s.%(msecs)03d %(module)s '
            '%(funcName)s:%(lineno)d] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )
        console.setFormatter(formatter)
    return logger


def setup_nerfstudio_viewer() -> Dict[str, Any]:
    """ Setup commands and and return a dictionary compatible
        with jupyter-server-proxy.
    """

    # Set logging
    logger = get_logger(__name__)
    logger.setLevel(logging.INFO)

    # We will use python's http.server to serve static content of the server
    http_server_executable = 'python'

    # Path to nerfsudio viewer app web root
    nerfsudio_web_root = os.path.join(sys.prefix, 'share', 'nerfstudio-viewer')

    def _get_icon_path():
        """Get the icon path"""
        return os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'icons',
            'nerfstudio-logo.svg'
        )

    def _nerfstudio_viewer_command(port, args):
        """Callable that we will pass to sever proxy to spin up
        nerfstudio viewer"""
        # Check if python executable is available
        executable = shutil.which(http_server_executable)
        if not executable:
            raise FileNotFoundError(
                f'{http_server_executable} executable not found.'
            )

        # Make nerfstudio viewer command arguments
        cmd_args = [
            'python', '-m', 'http.server',
            '--directory', nerfsudio_web_root, str(port)
        ]

        # If arguments like host, port are found in config, delete them.
        # We let Jupyter server proxy to take care of them
        # Additionally we delete path_prefix as well.
        for arg in ['--port', '--bind']:
            if arg in args:
                idx = args.index(arg)
                del args[idx:idx + 2]

        # Append user provided arguments to cmd_args
        cmd_args += args

        logger.info(
            'nerfstudio viewer will be launched with arguments %s', cmd_args
        )
        return cmd_args

    return {
        'command': _nerfstudio_viewer_command,
        'absolute_url': False,
        'timeout': 300,
        'new_browser_tab': True,
        'launcher_entry': {
            'enabled': True,
            'title': 'Nerfstudio Viewer',
            'num_instances': 1,
            'icon_path': _get_icon_path(),
            'category': 'Applications',
        }
    }
